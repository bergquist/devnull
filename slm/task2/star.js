var baseUri = 'https://lostinspace.lanemarknad.se:8000/api2/?session=1c026e4a-fa48-4459-b6f3-2e4f4ded5945'
        var showUri = baseUri + '&command=ship&arg=show'

        var container = $('#container')
        var planets = $('#planets')
        var ship = $('#ship')
        var interval = null;

        $.getJSON(baseUri + '&command=longrange', function(data) {
            data.stars.forEach(function(star, index) {
                $('<div></div>')
                    .addClass('system')
                    .css({
                        left: (star.x * 3) - 50,
                        top: (star.y * 3) - 50
                    })
                    .text(index + 1)
                    .appendTo(container)

                $('<li></li>')
                    .text((index + 1) + ': ' + star.name + ',  ' + star.class + ', ' + star.planets)
                    .appendTo(planets)
                    .click(function() {
                        navigateToStar(star)
                    })

                console.log(star)
            })
        })

        function navigateToStar(star) {
            var uri = baseUri + '&command=ship&arg=setunidest&arg2=' + star.name;
            $.getJSON(uri, startPolling)
        }

        function startPolling(star) {
            clearInterval(interval);
            interval = setInterval(function() {
                $.getJSON(showUri, function(data) {
                    setShipPosition(data.unix, data.uniy)

                    if (data.unix == star.x && data.uniy == star.y) {
                        clearInterval(interval)
                    }
                })
            }, 200)
        }

        function setShipPosition(x, y) {
            ship.css({
                left: x * 3,
                top: y * 3
            })
        }
