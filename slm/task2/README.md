Challenge:

It’s time to choose a star system to visit, to hunt for the origin of the signals. Right, those signals. Apparently the actual content is highly classified and your commanding officer back on Earth will only give you more details on a ‘need to know basis’.

Anyhoo, steering. Since much of the actual work is done by AI systems these days, all you have to do is point and click and the systems will take you at top speed to the star system you have chosen and break far out beyond the gravity well of the star. You current top speed isn’t too high, though..

So what you should do now is to either make each star selectable or each star name in the list, and then send the command to the server.

You will also need basic information about your ship, which is returned by the second API. Use that API to get progressive information about you position and status, so that you can plot the ship position on the star map. Do not call any server APIs more than 10 times/second combined.


Use the following API to select star system;

https://x.x.x.x:8000/api2/?session=174711&command=ship&arg=setunidest&arg2=cool%20star%20name


And this API to get basic ship information such as current universe x,y

https://x.x.x.x:8000/api2/?session=174711&command=ship&arg=show