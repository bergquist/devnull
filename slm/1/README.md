Challenge:

You and your crew have been selected from a pool of Earth’s finest and most talented astronauts. The recent boom in asteroid mining, providing an abundance of building material in orbit, together with the advent of the first working Alcubierre warp drive have within a span of years made it possible to create a fleet of starships.

The given target of the first mission of your starship and those of your colleagues and training partners is given - the first provingly alien signal to be detected ever, around the stars clustering Gliese 687.

Due to signal scattering from cosmic dust and partial occlusion from a recent nova, you only know the approximate region, so you need to visit the nearby stars in search of whom or what is the source of the signals.

Use the following API to scan nearby star systems and plot the stars graphically;

https://x.x.x.x:8000/api2/?session=174711&command=longrange

You should also list the names of the stars below the map for later selection.

To authenticate yourself to the ship you need to extract your devnull api key from the browser cookie of a logged in team member to use as ‘session’ variable. Note that the https api server uses a self-signed certificate.
