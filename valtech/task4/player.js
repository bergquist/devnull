var iconDiameter = 10;

var Player = function(startCell) {
  this.currentCell = startCell;
  this.marker = document.createElement('div');
  this.marker.classList.add('player-icon');
  document.getElementsByTagName('body')[0].appendChild(this.marker);

  this.moveMarker = function(cell) {
    this.marker.style.top = (cell.y * 90 + 45 - iconDiameter) + "px";
    this.marker.style.left = (cell.x * 90 + 45 - iconDiameter) + "px";
  }

  this.moveUp = function() {
    if (this.currentCell.walls.top) {
      this.currentCell = this.currentCell.walls.top;
      this.moveMarker(this.currentCell);
    }
  }

  this.moveDown = function() {
    if (this.currentCell.walls.bottom) {
      console.log(this.currentCell)
      this.currentCell = this.currentCell.walls.bottom;
      this.moveMarker(this.currentCell);
    }
  }

  this.moveLeft = function() {
    if (this.currentCell.walls.left) {
      this.currentCell = this.currentCell.walls.left;
      this.moveMarker(this.currentCell);
    }
  }

  this.moveRight = function() {
    if (this.currentCell.walls.right) {
      this.currentCell = this.currentCell.walls.right;
      this.moveMarker(this.currentCell);
    }
  }
}

var player = new Player(maze.grid[0][0]);

function doKeyDown (event) {
  switch (event.keyCode) {
    case 37:
      player.moveLeft();
      break;
    case 38:
      player.moveUp();
      break;
    case 39:
      player.moveRight();
      break;
    case 40:
      player.moveDown();
      break;
  }
}

window.addEventListener('keydown', doKeyDown, true);
