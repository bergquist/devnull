var fs = require('fs');
var jsdom = require('jsdom');

var html = fs.readFileSync('html.html', 'utf-8');
var jquery = "http://code.jquery.com/jquery.js";

var planetData = {};
jsdom.env(html, [ jquery ], parseHtml);
function parseHtml(errors, window) {
	var $ = window.$;

	planetData['planet name'] = $('#planet_name').text();

  $('#planet tr').each(function() {
  	var key = $(this).find('.section').text();
  	var value = $(this).find('.value').text();

  	if (key && value) {
  		planetData[key] = value;
  	}
  });

  console.log(planetData);
}