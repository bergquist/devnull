var GeneratePills = function(maze) {

  var h = maze.height;
  var w = maze.width;

  var pills = [];

  while(pills.length < 4) {
    var pill = {
      x: Math.floor(Math.random() * w),
      y: Math.floor(Math.random() * h)
    }

    if (pills.length == 0) {
      pills.push(pill);
    } else {
      var hit = false;
      pills.forEach(function(p) {
        if (p.x == pill.x && p.y == pill.y) hit = true;
      })

      if (!hit) pills.push(pill);
    }
  }

  pills.forEach(function(p) {
    maze.grid[p.x][p.y].pill = p;
  });

  return pills;
}


