var x = 0;
var y = 0;
var maze;

function init() {
	maze = new Maze(5, 5);
	var pills = GeneratePills(maze);
	renderMaze(maze);
	
	var pillsRenderer = new PillsRenderer(pills);
	pillsRenderer.render();
}

function renderMaze(maze) {
	for(var i in maze.grid) {
		var row = maze.grid[i];
		for(var k in row) {
			var tile = row[k];
			renderTile(tile);
		}
	}
}

function renderTile(tile) {
	var x = tile.x * (rect_side * 3)
	var y = tile.y * (rect_side * 3)
	renderUpperRow(tile, x, y);
	renderMiddleRow(tile, x, y + rect_side);
	renderLowerRow(tile, x, y + (rect_side * 2));
}

function renderUpperRow(tile, x, y) {
	renderWallPoint(x, y);
	if(tile.walls.top) {
		renderWalkPoint(x + rect_side, y);
	} else {
		renderWallPoint(x + rect_side, y);
	}
	renderWallPoint(x + (rect_side * 2), y);
}

function renderMiddleRow(tile, x, y) {
	if(tile.walls.left) {
		renderWalkPoint(x, y);
	} else {
		renderWallPoint(x, y);
	}
	renderWalkPoint(x + rect_side, y);
	if(tile.walls.right) {
		renderWalkPoint(x + (rect_side * 2), y);
	} else {
		renderWallPoint(x + (rect_side * 2), y);
	}
}

function renderLowerRow(tile, x, y) {
	renderWallPoint(x, y);
	if(tile.walls.bottom) {
		renderWalkPoint(x + rect_side, y);
	} else {
		renderWallPoint(x + rect_side, y);
	}
	renderWallPoint(x + (rect_side * 2), y);
}

function renderWallPoint(x, y) {
	renderPoint(x, y, '#ff0000')
}

function renderWalkPoint(x, y) {
	renderPoint(x, y, '#0000ff');
}

function renderPoint(x, y, color) {
	var context = maze_area.getContext('2d');
	context.beginPath();
    context.rect(x, y, rect_side, rect_side);
	context.fillStyle = color;
	context.fill();
	context.closePath();
}

init();
