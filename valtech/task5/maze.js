var Cell = function(x,y) {
  var x = x;
  var y = y;

  var walls = {
    top: undefined,
    left: undefined,
    bottom: undefined,
    right: undefined
  }

  var notVisited = function() {
    return !walls.top && !walls.bottom && !walls.left && !walls.right;
  }

  return {
    walls: walls,
    x: x,
    y: y,
    notVisited: notVisited
  }
}

var Maze = function(w, h) {
  var width = w;
  var height = h;
  var grid;

  grid = new Array(width);

  for(var i = 0; i < height; i++) {
    grid[i] = new Array(height);
  }

  for(var i = 0; i < height; i++) {
    var c = grid[i];

    for(var x = 0; x < width; x++) {
      c[x] = new Cell(i,x);
    }
  }

  var total = w * h;

  var startX = Math.floor(Math.random() * width);
  var startY = Math.floor(Math.random() * height);
  var visited = 1;
  var current = grid[startX][startY];

  var validNeighbours = function(cell) {
    var valid = [];

    var rightCell = grid[cell.x + 1] && grid[cell.x + 1][cell.y];

    if (rightCell && rightCell.notVisited()) {
      valid.push({ cell: rightCell, pos: "right" });
    }

    var leftCell = grid[cell.x - 1] && grid[cell.x -1][cell.y];
    if (leftCell && leftCell.notVisited()) { valid.push({ cell: leftCell, pos: "left" }); }

    var bottomCell = grid[cell.x] && grid[cell.x][cell.y -1];
    if (bottomCell && bottomCell.notVisited()) { valid.push( { cell: bottomCell, pos: "top" }); }

    var topCell = grid[cell.x] && grid[cell.x][cell.y + 1];
    if (topCell && topCell.notVisited()) { valid.push({cell: topCell, pos: "bottom" }); }

    return valid;
  }

  var cellStack = new Array();

  while(visited < total) {
    var n = validNeighbours(current);

    if (n.length > 0) {
      var nrandom = Math.floor(Math.random() * n.length);
      n = n[nrandom];

      switch(n.pos) {
        case "right":
          current.walls[n.pos] = n.cell;
          n.cell.walls.left = current;
          break;
        case "left":
          current.walls[n.pos] = n.cell;
          n.cell.walls.right = current;
          break;
        case "top":
          current.walls[n.pos] = n.cell;
          n.cell.walls.bottom = current;
          break;
        case "bottom":
          current.walls[n.pos] = n.cell;
          n.cell.walls.top = current;
          break;
      }

      cellStack.push(current);
      current = n.cell;
      visited++
    } else {
      current = cellStack.pop();
    }
  }

  return {
    grid: grid,
    height: h,
    width: w
  }
}
