var pillsDiameter =  rect_side / 5;

var PillsRenderer = function(pills) {
  this.pills = pills;

  this.render = function() {
	for(var i in this.pills) {
		var pill = this.pills[i];
		this.renderPill(pill);
	}
  }
  
  this.renderPill = function(pill) {
	var marker = document.createElement('div');
	marker.classList.add('pill-icon');
	marker.style.height = pillsDiameter * 2 + "px";
	marker.style.width = pillsDiameter * 2 + "px";
	marker.style.top = (pill.y * rect_side * 3) + (rect_side * 1.5) - pillsDiameter + "px"
	marker.style.left = (pill.x * rect_side * 3) + (rect_side * 1.5) - pillsDiameter + "px"
	document.getElementsByTagName('body')[0].appendChild(marker);
  }
}
