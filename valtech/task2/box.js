var Box = function() {
  this.x = 40;
  this.y = 160;
  this.height = 60;
  this.width = 10;
}

var box = new Box();

var drawBox = function(canvas, box) {
  var ctx = canvas.getContext("2d");
  ctx.clearRect(0,0, canvas.width, canvas.height);
  ctx.fillRect(box.x, box.y, box.width, box.height);
}

var canvas = document.getElementById('game');

drawBox(canvas, box);

function doKeyDown (event) {
  switch (event.keyCode) {
    case 38: /* up */
      var y = box.y - 10;
      box.y = y > 0 ? y : 0;
      break;
      case 40: /* down */
        var y = box.y + 10;
        box.y = y < canvas.height - (box.height-10) ? y : box.y;
    break;
  }

  drawBox(canvas, box);
}

window.addEventListener('keydown', doKeyDown, true);
