var fs = require('fs')
	, http = require('http')
	, socketio = require('socket.io');

var server = http.createServer(function(req, res) {
	res.writeHead(200, { 'Content-type': 'text/html'});
	res.end(fs.readFileSync(__dirname + '/test-client.html'));
}).listen(8080, function() {
	console.log('Listening at: http://localhost:8080');
});

var leftSideConnected = false;
var rightSideConnected = false;
socketio.listen(server).on('connection', function (socket) {
	socket.emit('status', 'connected');

	socket.on('status', function (msg) {
		console.log('status', msg);

		if (msg === 'LeftConnected') {
			leftSideConnected = true;
		} else if (msg === 'RightConnected') {
			rightSideConnected = true;
		}

		console.log(leftSideConnected, rightSideConnected);
		if (leftSideConnected && rightSideConnected) {
			var startData = {
				dx: Math.floor(Math.random() * 4) + 2,
				dy: Math.floor(Math.random() * 5) + 1,
				x: Math.floor((Math.random()*200)+200),
				y: Math.floor((Math.random()*200)+200)
			}

			var side = 'Left';
			socket.broadcast.emit('enter' + side, startData);
			console.log('enter' + side, startData);
		}
		console.log('Message Received: ', msg);
	});

	var areaWidth = 400;

	socket.on('leavingLeft', function(message) {
		var newPos = {
			dy: message.dy,
			dx: message.dx,
			x: message.x - (areaWidth),
			y: message.y
		}
		console.log('enterRight: ', newPos);
		socket.broadcast.emit('enterRight', newPos);
	});

	socket.on('leavingRight', function(message) {
		var newPos = {
			dy: message.dy,
			dx: message.dx,
			x: message.x + (areaWidth),
			y: message.y
		}

		console.log('enterLeft: ', newPos);
		socket.broadcast.emit('enterLeft', newPos);
	});

	socket.on('gameover', function(message) {
		var side = message === 'Left' ? 'Right' : 'Left';
		socket.broadcast.emit('victory' + side, 'You side WON!');
	});
});