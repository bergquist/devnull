var context;
var playground_size = 400;
var x = 300;
var y = 300;
var dx = 0;
var dy = 0;
var ball_diameter = 20
var side;
var interval;

var gotBalls = false;

var Box = function(x) {
  this.x = x;
  this.y = 160;
  this.height = 60;
  this.width = 10;
}

var box;

if (side == "Left") {
  box = new Box(40);
} else {
  box = new Box(playground_size - 40);
}

function init()	{
	window.socket = io.connect('http://10.200.0.69:8080');
	
	socket.on('enter' + side, function(data) {
		clearInterval(interval);
		gotBalls = true;
		dy = data.dy;
		dx = data.dx;
		x = data.x;
		y = data.y;
		interval = setInterval(draw, 30);
	});
	
	socket.on('victory' + side, function(data) {
		clearInterval(interval);
		gotBalls = false;
		draw();
		alert("You have won!");
	});
	
	socket.emit('status', side + "Connected");
	
	dx = Math.floor(Math.random() * 4) + 2;
	dy = Math.floor(Math.random() * 6);
	
	playground.width = playground_size;
	playground.height = playground_size;
	
  	context= playground.getContext('2d');
}

function box_hit_bounce() {
	if(box.y < y && y < (box.y + box.height)) {
		dx = -dx;
	} else {
		socket.emit("gameover", side);
		clearInterval(interval);
		gotBalls = false;
		draw();
		alert("You lost!");
	}
}

function leaving_playground() {
	socket.emit('leaving' + side, {dy: dy, dx: dx, x: x, y: y});
	gotBalls = false;
	dx = -dx;
}

function draw()	{
	context.clearRect(0,0, playground_size, playground_size);
	drawBall();
	
	context.fillRect(box.x, box.y, box.width, box.height);
}

function drawBall() {
	if(!gotBalls) { return; }
	
	context.beginPath();
	context.fillStyle="#0000ff";
	context.arc(x, y, ball_diameter ,0 ,Math.PI * 2, true);
	context.closePath();
	context.fill();
	
	if (side === "Left"){
		var hit = ball_diameter + box.x + box.width;
	} else {
		var hit = 50;
	}
	
	if(side === "Left") {
		if (x < hit){
			box_hit_bounce();
		}

		if (x > playground_size){
			leaving_playground();
		}
	} else {
		if (x < 0){
			leaving_playground();
		}

		if (x > playground_size - hit){
			box_hit_bounce();
		}
	}
	
	
	if( y < ball_diameter || y > playground_size - ball_diameter) dy = -dy;
	
	x += dx;
	y += dy;
}

function doKeyDown (event) {
  switch (event.keyCode) {
    case 38: /* up */
      var y = box.y - 10;
      box.y = y > 0 ? y : 0;
      break;
      case 40: /* down */
        var y = box.y + 10;
        box.y = y < playground.height - (box.height-10) ? y : box.y;
    break;
  }
}

window.addEventListener('keydown', doKeyDown, true);
