var Cell = function(x,y) {

  var x = x, y = y;

  var walls = {
    top: true,
    left: true,
    bottom: true,
    right: true
  }

  this.visited = function() {
    return walls.top && walls.bottom && walls.left && walls.right;
  }
}

var Maze = function(w, h) {
  var width = w;
  var height = h;
  var grid;

  grid = new Array(width);

  for(var i = 0; i < height; i++) {
    grid[i] = new Array(height);
  }

  for(var i = 0; i < height; i++) {
    var c = grid[i];

    for(var x = 0; x < width; x++) {
      c[x] = new Cell(i,x);
    }
  }

  var total = w * h;
  
  var startX = Math.floor(Math.random() * width);
  var startY = Math.floor(Math.random() * height);
  
  var current = grid[startX][startY];

  console.log(current);

  return {
    grid: grid
  }
}

var c = new Cell(1,1);
var m = new Maze(5,5);

console.log(c.visited());
console.log(m.grid);

