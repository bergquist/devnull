var Tile = function(color, x, y) {
  var color = color;
  var height = 50;
  var width = 50;
  var x = x;
  var y = y;

  return {
    x: x,
    y: y,
    color: color,
    height: height,
    width: width
  }
}

var Pacman = function(canvas) {
  var context = canvas.getContext("2d");
  var colors = ["#f00","#00f"];
  var grid = new Array();

  this.current = function() { return grid; }

  this.build= function() {
    for(var i = 0; i < 5; i++) {
      grid[i] = new Array();
      for (var x = 0; x < 5; x++) {
        var tile = new Tile(randomColor(), i, x);
        grid[i].push(tile);
      }
    } 
  }

  this.draw = function() {
    for(var i = 0; i < 5; i++) {
      var tiles = grid[i];
      
      for(var x = 0; x < 5; x++) {
        drawTile(tiles[x]);
      }
    }
  }

  var drawTile = function(tile) {
    context.fillStyle = tile.color;
    var h = tile.height;
    var w = tile.width;
    context.fillRect(tile.x * h, tile.y *w, h, w);
  }

  var randomColor = function() {
    var r = Math.floor(Math.random() * 2);
    return colors[r];
  }
}


