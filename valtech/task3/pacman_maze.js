// up 1
// right 2
// down 4
// left 8
var first_tile = {tile: [
	[1, 0, 1],
	[1, 0, 1],
	[1, 1, 1],
], up: true};

var second_tile = {tile: [
	[1, 0, 1],
	[0, 0, 0],
	[1, 1, 1],
], up: true, left:true, right:true};

var third_tile = {tile: [
	[1, 0, 1],
	[0, 0, 0],
	[1, 0, 1],
], up: true, down: true, left: true, right: true};

var fourth_tile = {tile: [
	[1, 0, 1],
	[1, 0, 1],
	[1, 0, 1],
], up: true, down: true};

var newTile = {
	walls: {
		up: true,
		right: true,
		down: true,
		left: true,
	}
}

var tiles = [first_tile, second_tile, third_tile, fourth_tile];
var x = 0;
var y = 0;
var rect_side = 30;
var tilesSide = 5;
var randomTiles = new Array();

function init() {
	for(var i = 0; randomTiles.length < (tilesSide * tilesSide); i++) {
		var tileIndex = Math.floor(Math.random() * tiles.length);
		var previous = randomTiles[i - 1];
		var previousDocking = false;
		var aboveDocking = false;
		var firstOK = false;
		var tile = tiles[tileIndex];
		if(previous) {
			previousDocking = checkPreviousDocking(previous, tile);
		}
		
		var above = randomTiles[i - tilesSide];
		if(above) {
			aboveDocking = checkAboveDocking(above, tile);
		}
		
		if(aboveDocking || previousDocking || i == 0) {
			randomTiles.push(tile);
			renderTile(tile.tile);
			var test = randomTiles.length;
			if((test % tilesSide) == 0){
				y = 0;
				x = (test / tilesSide) * 90;
			}
		}
	}
}

function checkPreviousDocking(previous, current) {
	previous.right && current.left;
}

function checkAboveDocking(above, current) {
	return above.down && current.up;
}

function renderTile(tile) {
	for(var i in tile) {
		var row = tile[i];
		renderRow(row, i);
	}
	y = y + (tile.length * rect_side);
}

function renderRow(row, i) {
	var context = maze_area.getContext('2d');
	var xInc = x;
	var yInc = y + (i * rect_side)
	for(var i in row) {
		var val = row[i];
		context.beginPath();
	    context.rect(xInc, yInc, rect_side, rect_side);
		if(val == 1) {
		    context.fillStyle = '#ff0000';
		} else {
			context.fillStyle = '#0000ff';
		}
	    context.fill();
		context.closePath();
		xInc = xInc + rect_side;
	}
}