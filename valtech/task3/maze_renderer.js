var x = 0;
var y = 0;
var rect_side = 30;

function init() {
	var maze = new Maze(5, 5);
	console.log(maze.grid.length);
	
	for(var i in maze.grid) {
		var row = maze.grid[i];
		for(var k in row) {
			var tile = row[k];
			renderTile(tile);
		}
	}
}

function renderTile(tile) {
	var x = tile.x * (rect_side * 3)
	var y = tile.y * (rect_side * 3)
	renderUpperRow(tile, x, y);
	renderMiddleRow(tile, x, y + rect_side);
	renderLowerRow(tile, x, y + (rect_side * 2));
}

function renderUpperRow(tile, x, y) {
	renderWallPoint(x, y);
	if(tile.walls.top) {
		renderWallPoint(x + rect_side, y);
	} else {
		renderWalkPoint(x + rect_side, y);
	}
	renderWallPoint(x + (rect_side * 2), y);
}

function renderMiddleRow(tile, x, y) {
	if(tile.walls.left) {
		renderWallPoint(x, y);
	} else {
		renderWalkPoint(x, y);
	}
	renderWalkPoint(x + rect_side, y);
	if(tile.walls.right) {
		renderWallPoint(x + (rect_side * 2), y);
	} else {
		renderWalkPoint(x + (rect_side * 2), y);
	}
}

function renderLowerRow(tile, x, y) {
	renderWallPoint(x, y);
	if(tile.walls.bottom) {
		renderWallPoint(x + rect_side, y);
	} else {
		renderWalkPoint(x + rect_side, y);
	}
	renderWallPoint(x + (rect_side * 2), y);
}

function renderWallPoint(x, y) {
	renderPoint(x, y, '#ff0000')
}

function renderWalkPoint(x, y) {
	renderPoint(x, y, '#0000ff');
}

function renderPoint(x, y, color) {
	var context = maze_area.getContext('2d');
	context.beginPath();
    context.rect(x, y, rect_side, rect_side);
	context.fillStyle = color;
	context.fill();
	context.closePath();
}