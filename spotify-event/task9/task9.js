
var foo = {}, bar = {};

var addMethods = 0;
function addFunc(obj) {
	obj.newFunc = function() {
		return ++addMethods;
	}
	return obj;
}

foo = addFunc(foo);
bar = addFunc(bar);

console.log(foo.newFunc() === 1);
console.log(bar.newFunc() === 2);

