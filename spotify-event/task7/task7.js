function quirky(cb, input) {
	setTimeout(function() {
			cb(input);
	}, Math.random()*100);
}

function caller(cb) {
	var returns = 0;

	for(var i = 0; i < 10;i++) {
		quirky(function() {
			returns += 1;
			console.log(returns);
			if (returns === 10) {
				cb();
			}
		}, i);
	}
}

caller(function() {
	console.log('DONE');
})